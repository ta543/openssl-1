#!/bin/bash

# Define the project root directory
project_root="openssl_project"

# Create directory structure
echo "Creating directory structure..."
mkdir -p $project_root/{bin,src,tests,docs,certs,configs}

# Creating initial files and scripts
echo "Creating initial files..."

# Dockerfile
cat > $project_root/Dockerfile <<EOF
# Use an official Ubuntu base image
FROM ubuntu:latest

# Install OpenSSL and Nginx
RUN apt-get update && apt-get install -y openssl nginx

# Set the working directory
WORKDIR /openssl

# Copy the entry script
COPY bin/entry.sh /openssl

# Make the script executable
RUN chmod +x /openssl/entry.sh

# Default command
CMD ["/openssl/entry.sh"]
EOF

# Entry script for Docker
cat > $project_root/bin/entry.sh <<EOF
#!/bin/bash
# Placeholder for server start-up or other command
echo "OpenSSL Docker container has been started"
# Keep container running
tail -f /dev/null
EOF

# Python script to generate Nginx configuration files
cat > $project_root/src/nginx_config_generator.py <<EOF
import sys

def generate_nginx_config(domain, cert_path, key_path):
    config_template = f"""
server {{
    listen 443 ssl;
    server_name {domain};

    ssl_certificate {cert_path};
    ssl_certificate_key {key_path};

    ssl_session_cache shared:SSL:1m;
    ssl_session_timeout  10m;
    ssl_ciphers 'ECDHE-ECDSA-AES128-GCM-SHA256:ECDHE-RSA-AES128-GCM-SHA256:ECDHE-ECDSA-AES256-GCM-SHA384:ECDHE-RSA-AES256-GCM-SHA384:DHE-RSA-AES128-GCM-SHA256:DHE-RSA-AES256-GCM-SHA384:ECDHE-ECDSA-CHACHA20-POLY1305:ECDHE-RSA-CHACHA20-POLY1305:DHE-RSA-CHACHA20-POLY1305';
    ssl_prefer_server_ciphers on;

    location / {{
        root /usr/share/nginx/html;
        index index.html index.htm;
    }}
}}
"""
    return config_template

def main():
    if len(sys.argv) != 4:
        print("Usage: python nginx_config_generator.py <domain> <cert_path> <key_path>")
        sys.exit(1)

    domain = sys.argv[1]
    cert_path = sys.argv[2]
    key_path = sys.argv[3]

    config = generate_nginx_config(domain, cert_path, key_path)
    with open("/etc/nginx/sites-available/default", "w") as file:
        file.write(config)

    print("Nginx configuration file has been updated.")

if __name__ == "__main__":
    main()
EOF

# Python script for testing SSL connection
cat > $project_root/tests/test_ssl_configuration.py <<EOF
import ssl
import socket
import sys

def test_ssl_connection(host, port):
    context = ssl.create_default_context()

    with socket.create_connection((host, port)) as sock:
        with context.wrap_socket(sock, server_hostname=host) as ssock:
            data = ssock.recv(1024)
            print(f"Received: {data.decode('utf-8')}")

def main():
    if len(sys.argv) != 3:
        print("Usage: python test_ssl_configuration.py <host> <port>")
        sys.exit(1)

    host = sys.argv[1]
    port = int(sys.argv[2])

    try:
        test_ssl_connection(host, port)
        print("SSL connection established successfully.")
    except Exception as e:
        print(f"Failed to establish SSL connection: {e}")

if __name__ == "__main__":
    main()
EOF

# Create documentation files
echo "# Project Overview" > $project_root/docs/README.md
echo "# Setup Instructions" > $project_root/docs/SETUP.md
echo "# Troubleshooting Guide" > $project_root/docs/TROUBLESHOOTING.md

# Create placeholder Nginx and OpenSSL configuration files
echo "server {}" > $project_root/configs/nginx.conf
echo "# OpenSSL configuration settings" > $project_root/configs/openssl.cnf

# Make all scripts executable
find $project_root/bin -type f -iname "*.sh" -exec chmod +x {} \;
find $project_root/src -type f -iname "*.py" -exec chmod +x {} \;
find $project_root/tests -type f -iname "*.py" -exec chmod +x {} \;

echo "Project setup complete."
