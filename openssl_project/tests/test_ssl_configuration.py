import ssl
import socket
import sys

def test_ssl_connection(host, port):
    context = ssl.create_default_context()

    with socket.create_connection((host, port)) as sock:
        with context.wrap_socket(sock, server_hostname=host) as ssock:
            data = ssock.recv(1024)
            print(f"Received: {data.decode('utf-8')}")

def main():
    if len(sys.argv) != 3:
        print("Usage: python test_ssl_configuration.py <host> <port>")
        sys.exit(1)

    host = sys.argv[1]
    port = int(sys.argv[2])

    try:
        test_ssl_connection(host, port)
        print("SSL connection established successfully.")
    except Exception as e:
        print(f"Failed to establish SSL connection: {e}")

if __name__ == "__main__":
    main()
