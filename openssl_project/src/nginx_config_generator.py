import sys

def generate_nginx_config(domain, cert_path, key_path):
    config_template = f"""
server {{
    listen 443 ssl;
    server_name {domain};

    ssl_certificate {cert_path};
    ssl_certificate_key {key_path};

    ssl_session_cache shared:SSL:1m;
    ssl_session_timeout  10m;
    ssl_ciphers 'ECDHE-ECDSA-AES128-GCM-SHA256:ECDHE-RSA-AES128-GCM-SHA256:ECDHE-ECDSA-AES256-GCM-SHA384:ECDHE-RSA-AES256-GCM-SHA384:DHE-RSA-AES128-GCM-SHA256:DHE-RSA-AES256-GCM-SHA384:ECDHE-ECDSA-CHACHA20-POLY1305:ECDHE-RSA-CHACHA20-POLY1305:DHE-RSA-CHACHA20-POLY1305';
    ssl_prefer_server_ciphers on;

    location / {{
        root /usr/share/nginx/html;
        index index.html index.htm;
    }}
}}
"""
    return config_template

def main():
    if len(sys.argv) != 4:
        print("Usage: python nginx_config_generator.py <domain> <cert_path> <key_path>")
        sys.exit(1)

    domain = sys.argv[1]
    cert_path = sys.argv[2]
    key_path = sys.argv[3]

    config = generate_nginx_config(domain, cert_path, key_path)
    with open("/etc/nginx/sites-available/default", "w") as file:
        file.write(config)

    print("Nginx configuration file has been updated.")

if __name__ == "__main__":
    main()
